# 1.4.Vue.js

以官網的風格指南為主要參考 https://cn.vuejs.org/v2/style-guide/index.html。請盡量符合官網「優先級A」及「優先級B」之規則，詳述如下：

## 一、Component規範

### .vue檔命名（優先級C，2020/1/3）

1. 檔名格式為大駝峰，組件名稱和template當中都相同為大駝峰（2020/1/6修正）。
   
   參考並稍做修改：https://cn.vuejs.org/v2/style-guide/index.html#%E5%8D%95%E6%96%87%E4%BB%B6%E7%BB%84%E4%BB%B6%E6%96%87%E4%BB%B6%E7%9A%84%E5%A4%A7%E5%B0%8F%E5%86%99-%E5%BC%BA%E7%83%88%E6%8E%A8%E8%8D%90

2. 組件名稱為多個單詞的組成。
   
   參考並稍做修改：https://cn.vuejs.org/v2/style-guide/index.html#%E7%BB%84%E4%BB%B6%E5%90%8D%E4%B8%BA%E5%A4%9A%E4%B8%AA%E5%8D%95%E8%AF%8D-%E5%BF%85%E8%A6%81

3. 通用性的名稱要使用App開頭。
   
   參考並稍做修改：https://cn.vuejs.org/v2/style-guide/index.html#%E5%9F%BA%E7%A1%80%E7%BB%84%E4%BB%B6%E5%90%8D-%E5%BC%BA%E7%83%88%E6%8E%A8%E8%8D%90

4. 具有唯一性且通常是無prop的Component名稱前要加上The，比如TheSidebar。一般是用在特定頁面才會使用（非共用的）、較大區塊的Component。

   以一個具體範例來解決。假設有一個登入頁面，有三種模式會切換表單內容，分別有登入、註冊、忘記密碼，將這三種表單做成Component所以會有三個.vue檔，這三個Component具有唯一性（只在登入頁使用）所以命名時會在前面加上"The"。
   
   參考並稍做修改：https://cn.vuejs.org/v2/style-guide/index.html#%E5%8D%95%E4%BE%8B%E7%BB%84%E4%BB%B6%E5%90%8D-%E5%BC%BA%E7%83%88%E6%8E%A8%E8%8D%90
   
5. 緊密耦合的子組件名稱要以父組件名稱作為前綴。
   
   參考並稍做修改：https://cn.vuejs.org/v2/style-guide/index.html#%E7%B4%A7%E5%AF%86%E8%80%A6%E5%90%88%E7%9A%84%E7%BB%84%E4%BB%B6%E5%90%8D-%E5%BC%BA%E7%83%88%E6%8E%A8%E8%8D%90

6. 組件名稱應傾向於完整名稱而不是縮寫。
   
   參考並稍做修改：https://cn.vuejs.org/v2/style-guide/index.html#%E5%AE%8C%E6%95%B4%E5%8D%95%E8%AF%8D%E7%9A%84%E7%BB%84%E4%BB%B6%E5%90%8D-%E5%BC%BA%E7%83%88%E6%8E%A8%E8%8D%90

### prop（優先級C，2020/1/3）

1. 在聲明prop的時候使用小駝峰，在template中也是使用小駝峰（2020/1/6修正）。
   
   參考並稍做修改：https://cn.vuejs.org/v2/style-guide/index.html#%E5%AE%8C%E6%95%B4%E5%8D%95%E8%AF%8D%E7%9A%84%E7%BB%84%E4%BB%B6%E5%90%8D-%E5%BC%BA%E7%83%88%E6%8E%A8%E8%8D%90

2. prop定義應盡量詳細，至少要包含required及type。依情況加上default及validator為佳。
   
   參考並稍做修改：https://cn.vuejs.org/v2/style-guide/index.html#Prop-%E5%AE%9A%E4%B9%89-%E5%BF%85%E8%A6%81
   
3. 並要加上註解。

4. 組件內禁止直接修改prop；如果有需要使用雙向綁定，需搭配使用.sync或v-model的方式。

### options的順序（優先級C，2020/1/6）

參考：https://cn.vuejs.org/v2/style-guide/#%E7%BB%84%E4%BB%B6-%E5%AE%9E%E4%BE%8B%E7%9A%84%E9%80%89%E9%A1%B9%E7%9A%84%E9%A1%BA%E5%BA%8F-%E6%8E%A8%E8%8D%90

1. Side Effects (觸發組件外的影響)

   el

2. 全域感知 (要求組件外的知識)

   name

   parent

3. 組件類型 (更改組件的類型)

   functional

4. 模板修改器 (改變模板的編譯方式)

   delimiters

   comments

5. 模板依賴 (模板內使用的資源)

   components

   directives

   filters

6. 組合 (向選項裡合併屬性)

   extends

   mixins

7. 接口 (組件的接口)

   inheritAttrs

   model

   props/propsData

8. 本地狀態 (本地的響應式屬性)

   data

   computed

9. 事件 (通過響應事件觸發的callback)

   watch

10. 生命週期鉤子 (按照他們被調用的順序)

   beforeCreate

   created

   beforeMount

   mounted

   beforeUpdate

   updated

   activated

   deactivated

   beforeDestroy

   destroyed

11. 非響應式的屬性 (不依賴響應系統的實例屬性)

   methods

12. 渲染 (組件輸出的聲明式描述)

   template/render

   renderError

### 模板html的空行（優先級C，2020/1/6、最後修正2020/2/10）

當元素使用兩個以上的vue特之後應換行

```
<input
  :value="newTodoText"
  :placeholder="newTodoInstructions">
```

應避免以下的換行風格（如prettier），這個寫法會無法將標籤內的內容折疊。

```
<input
  :value="newTodoText"
  :placeholder="newTodoInstructions"
>
```
