# 2.4.API

## 一、使用axios

使用axios

https://github.com/axios/axios

## 二、建立獨立的api控制器

建立獨立的api控制器

## 三、範例

1. api主檔（apis/index.js）

```js
import { Message } from 'element-ui'
import apiService from './apiService.js' // 帳號api service


const successMessage = (message) => {
  Message.success({
    message
  })
}

// ---------- 帳號 ----------
// 取得使用者資訊
export function user (params) {
  return apiService({
    url: '/api/v1/user',
    method: 'get',
    params
  }).then(d => d).catch(d => d)
}
// 個人註冊
export function userRegister (data) {
  return apiService({
    url: '/api/v1/user.register',
    method: 'post',
    data
  }).then(d => {
    successMessage('註冊成功')
  }).catch(d => d)
}
```

2. api service（apis/apiService.js）

一個service對應一個server網址，serivce當中設定api服務相關的設定、以及使用api攔截器設定api發送前及取得結果後要執行的任務。

```js
import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'

/**
 * 帳號api service
 */

// axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_CLOUD_API_URL, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  // timeout: 5000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    if (store.state.jwtToken) {
      config.headers['Authorization'] = `Bearer ${store.state.jwtToken}`
    }
    return config
  },
  error => {
    if (process.env.VUE_APP_MODE === 'serve') {
      console.log(error) // for debug
    }
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data
    // 有回傳但success不為true
    if (!res.success) {
      let showMessage = ''
      let backendMessage = store.state.errorCodes[res.message] // 比對錯誤代碼表
      if (backendMessage) {
        showMessage = backendMessage
      } else if (res.message) {
        showMessage = res.message // 後端的錯誤訊息（不在代碼表裡）
      } else {
        showMessage = '未知錯誤，請確認網路狀態或聯絡系統管理員'
      }
      Message({
        message: showMessage,
        type: 'error',
        duration: 5 * 1000
      })

      return Promise.reject(res.message || 'Error')
    }
    
    return res
  },
  error => {
    if (process.env.VUE_APP_MODE === 'serve') {
      console.log(error) // for debug
    }
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service

```