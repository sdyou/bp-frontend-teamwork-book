# 架設Nuxt SSR專案

## 一、如何從 Vue 遷移到 Nuxt (2020/01)

### 使用說明

這份文件是在撰寫的時候保證是最新的內容，但是隨著時間的推移將會變得跟實際上的做法有出入，如果你發現這份文件無法滿足你的需求，可以直接參考官網，或是選擇編輯這份文件。

### 第一步-安裝

方案 A

使用現成框架 donuxt 做開發(個人推薦)

方案 B

使用官方推薦的 create-nuxt-app

使用 A 的優點是裡面會有已經寫好的範例可供參考。

使用方案 B 的話它會問你要裝哪一些東西，以下是我推薦的選擇

![](img/nuxt-ssr_1.png)

package manager 就是個人喜好問題

ui 應該都是 element 

server framework 選推薦的 None

nuxt modules 選 axios 打 api、dotenv 是用來讀取 .env 檔案用的套件(for server side)

linting tool 我都沒選 因為看了很煩 相信我

測試框架選 jest 因為好用、市占率高

rendering mdoe 有 spa 跟 ssr ，如果需求是要做無障礙或是 SEO 的話就選 ssr 反之選 spa

jsconfig.json 因為他推薦所以我有選 雖然不識很清楚幹嘛的

### 第二步-搬檔案

這邊分為兩類檔案

第一類-非預設存在的資料夾(utils、const、mocks、mixins……)

將你在開發中自行添加的那些檔案及資料夾全部複製一份丟過去 Nuxt。例如你在 src 下添加了 utils、const 這兩個不在預設就有的資料夾，那妳可以直接把他們複製過去 Nuxt 目錄下。

第二類-預設就存在的資料夾(components、assets、store……)

如果是 components、assets 這種本來就有的資料夾就把在 Vue 那邊寫的東西通通丟進 Nuxt 的相對應的資料夾下。

另外有一點不一樣的是，在 Vue 裡面叫 views 的資料夾，在Nuxt 裡面叫做 pages，所以也是按照第二類的做法。

如果在錯誤訊息中有出現找不到檔案的話通常就是因為你漏搬了某些檔案，只要搬完讓檔案能順利載入就可以解決因為找不到檔案而產生的錯誤。

檔案搬過去後，若要沿用本來的css，則需要在nuxt.config.js中去設定css(or plugins)的路徑，而若要沿用在舊的app.vue中的架構(如header, footer)，需要搬到後來的layout/default.vue裡。

### 第三步-解決錯誤

這一步的動作旨在解決所有錯誤，讓我們可以看到一個至少有畫面的網頁

#### scss

方案 A:  無須調整

方案 B:  cli 並不會詢問你要不要安裝 sass 編譯器，需使用以下指令安裝依賴套件

`$ npm install --save-dev node-sass sass-loader`

注意如果在 Vue 裡面寫了全域的 scss 的話，在 Nuxt 的 nuxt.config.js 中的 css 裡面寫入可以達到一樣的效果

#### router

由於 Nuxt 的 router 是由 pages 的目錄結構動態產生的，也就是說 Nuxt 本身是不需要你自己手動寫 router.js 的，但是通常從 Vue 開始的專案的目錄結構直接丟去 Nuxt 所產生出來的 router 一定跟原本自己手寫的那個 router 長得不一樣。

這時候如果我們想改變目錄結構來自動產生跟原本一樣的 router 表的話將會是一件不明智的事情(因為這個可能會是一件大工程，看你的專案有多大，專案越大、頁面越多就會改起來很累)

所以我會推薦使用 @nuxtjs/router ，他能讓 Nuxt 像 Vue 一樣的使用 vue-router，詳細使用方始就請參考官方文件 

請注意，如果使用 @nuxtjs/router ，那麼在 router 裡面的 lazy load component 的方式是不支援的

```js
{
  path: '/foo',
  name: 'foo',
  component: () => import('./Foo.vue'),
}
```

必須先 import component 

```js
import Foo from '@/pages/Foo.vue'
...
{
  path: '/foo',
  name: 'foo',
  component: Foo,
}
store(vuex)
```

如果套用現有寫好的 store 我推薦可以用 經典模式 
如果是還沒開始寫 js 的話可以直接參考官網的範例

只在客户端使用的插件

通常在 Vue 專案裡面我們會用到很多只能夠在客戶端使用的插件，這種情況下下，你可以用
 mode: 'client’，使得插件只會在客戶端運行。

nuxt.config.js:

```js
export default {
  plugins: [
    { src: '~/plugins/both-sides.js' },
    { src: '~/plugins/client-only.js', mode: 'client' },
    { src: '~/plugins/server-only.js', mode: 'server' }
  ]
}
```

### window or document is not defined

Nuxt 使用 Node.js 作為 server 提供靜態的網頁內容，但是 Node.js 上面是沒有 window/document 這兩個瀏覽器 API 的，所以在讓何情況下只要讓 serever side 有操作到 window/documen 的話都會噴錯誤

解決方式有幾種

1. 找出所有程式(不包含 node_modules)裡面寫的 window/document 並在他們之前使用 
```js
if(process.client) { 
  // 移到這裡面來 
}
```
2. 如果是寫在 created 之前(包含created)的 lifecycle 裡面的話，可以全部移到 mounted 裡面做，因為 mounted 就完全是在 client 裡面運行的
3. 如果還有剩下的此類錯誤，就請參考上面的段落 <只在客户端使用的插件>中的說明，將套件改為 client only 就可以解決此類錯誤


### 環境變數

在 Nuxt 裡面不像 Vue 可以隨意讀取各種 .env.testing .nev.staging 這種檔案 
@nuxt/dotenv 這個套件預設只讀取根目錄擋下的 .env (也許是我技能差還沒研究出來可以的方法?)
所以這時候我們就只好每次第一次部屬不同環境的時候手動修改 .env 裡面的內容了

### 其他

如果這份文件並沒有提到你想解決某項問題時，你可以到 https://zh.nuxtjs.org/faq 查看有沒有解答
