# 2.5.Vuex

## 一、資料夾及結構：

1. vuex存放於src/store裡面。
2. vuex模組存於src/store/modules資料夾內。
3. 模組命名方式基本上沒有嚴格規範，可以依照功能命名、或者作用域（同頁面名稱）命名皆可。
4. 在較複雜的專案當中，可將options(actions/mutations...)分別採分成獨立的檔案，並改用資料夾來整理（如下範例二）。

### 範例一：較簡單的結構

```
  ├──store
  |   ├──modules
  |   |   ├──accounts.js
  |   |   └──products.js
  |   └──index.js
```

### 範例二：較複雜的結構（將options拆開）

```
  ├──store
  |   ├──modules
  |   |   ├──accounts
  |   |   |   ├──actions.js
  |   |   |   ├──getters.js
  |   |   |   ├──mutations.js
  |   |   |   ├──state.js
  |   |   |   └──index.js  
  |   |   └──products
  |   |       ├──actions.js
  |   |       ├──getters.js
  |   |       ├──mutations.js
  |   |       ├──state.js
  |   |       └──index.js  
  |   ├──actions.js
  |   ├──getters.js
  |   ├──mutations.js
  |   ├──state.js
  |   └──index.js

```

## 二、options的宣告順序

如果options是放在同一支檔案裡，需依照以下順序（此順序同官網範例）：

1. state
2. getters
3. actions
4. mutations

## 三、命名原則

1. vuex相關檔案皆使用小駝峰命名
2. 皆以options名稱當前綴，state除外，且不加s；例：在actions時，取名需用action開頭；在mutations，取名需以mutation開頭，在getters時，取名需用getters開頭。
3. 同種功能，盡量使用同樣的前輟，後續維護會較方便(以state為名)。例：帳號資訊 actionAccountInfo, mutatioAccountInfo, accountInfo, getterAccountInfo
 
### 例：麵包屑 

```js
state: {
	breadCrumbPath: 'xxxxxxxxxxxxxxxxxx',
	breadCrumbName: '',
},
getters: {
	getterBreadCrumbPath(state, getters, rootState, rootGetters) {
    return state.breadCrumbPath
  },
	breadCrumbName(state, getters, rootState, rootGetters) {
    return state.breadCrumbName
  },
},
actions:{
	async actionBreadCrumb ({ dispatch, commit, state }, payload) {
		let res = await api.xxxxxx
		.
		.
		.
		.
		
		commit('mutationBreadCrumbPath', data)
		commit('mutationBreadCrumbName', data)
	}
},
mutations:{
	mutationBreadCrumbPath (state, payload){
		state.breadCrumbPath = payload
	},
	mutationBreadCrumbName (state, payload) {
		state.breadCrumbName = payload
	}
}
```

## actions及mutations差異及規範

1. 非同步行為（打api）應放在action
2. 更改state，應放在mutations

