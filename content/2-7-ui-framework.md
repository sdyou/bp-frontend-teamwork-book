# 2.5.UI框架

## element-ui

- 官網：https://element.eleme.io/#/zh-CN
- 簡易安裝方式：https://github.com/ElementUI/vue-cli-plugin-element

### 載入element-ui
```
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-TW'

Vue.use(ElementUI, { locale }) // 設定語言為繁中
```
