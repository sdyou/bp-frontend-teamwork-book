# 3.1.1.畫面截圖

## 一、說明

### 適用範圍

1. html to canvas

2. canvas to base64

3. canvas to image並下載

4. canvas to pdf並下載

### 依賴套件

html2canvas、canvg、rgbcolor、jspdf

## 二、範例

```javascript
import html2canvas from 'html2canvas'
import canvg from 'canvg'
import jsPDF from 'jspdf'

// 截取畫面轉為base64
export async function getScreenCanvas (selector) {
  try {
    // -- svg轉canvas --
    let nodesToRecover = []
    let nodesToRemove = []
    let svgElem = document.querySelectorAll(selector + ' svg')
    if (svgElem && svgElem.length) {
      for (let i = 0; i++; i < i.length) {
        let node = svgElem[i]
        let parentNode = node.parentNode
        let svg = node.outerHTML.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '') // 等同.trim()

        let canvas = document.createElement('canvas')
        canvas.width = 650
        canvas.height = 480
        if (node.style.position) {
          canvas.style.position += node.style.position
          canvas.style.left += node.style.left
          canvas.style.top += node.style.top
        }
        if (node.clientWidth) {
          canvas.width = node.clientWidth
        }
        if (node.clientHeight) {
          canvas.height = node.clientHeight
        }
        if (node.className) {
          canvas.className = node.className
        }
        canvg(canvas, svg)

        nodesToRecover.push({
          parent: parentNode,
          child: node
        })
        parentNode.removeChild(node)

        nodesToRemove.push({
          parent: parentNode,
          child: canvas
        })

        parentNode.appendChild(canvas)
      }
    }

    const canvas = await html2canvas(document.querySelector(selector), {
      background: '#fff'
    })

    // -- svg還原 --
    if (nodesToRemove.length && nodesToRecover.length) {
      nodesToRemove.forEach(d => {
        d.parent.removeChild(d.child)
      })
      nodesToRecover.forEach(d => {
        d.parent.appendChild(d.child)
      })
    }

    return Promise.resolve(canvas)
    
  } catch (e) {
    // throw new Error(e)
    return Promise.reject(null)
  }
}

// 回傳base64圖片
export async function getScreenBase64 (selector) {
  const canvas = await getScreenCanvas(selector)

  if (canvas) {
    return Promise.resolve(canvas.toDataURL('image/jpeg').replace('image/jpeg', 'image/octet-stream'))
  } else {
    return Promise.reject(null)
  }
}

// 下載圖片
export async function downloadScreenImage (selector, fileName = 'content') {
  const canvas = await getScreenCanvas(selector)

  let img = document.createElement('a')
  img.href = canvas.toDataURL('image/jpeg').replace('image/jpeg', 'image/octet-stream')
  img.download = `${fileName}.jpg`
  img.click()

  return Promise.resolve(true)
}

// 下載pdf
export async function downloadScreenPdf (selector, fileName = 'content') {
  const canvas = await getScreenCanvas(selector)

  var contentWidth = canvas.width;
  var contentHeight = canvas.height;
  var padding = 20; // 邊距

  //一页pdf显示html页面生成的canvas高度;
  var pageHeight = contentWidth / 592.28 * 841.89;
  //未生成pdf的html页面高度
  var leftHeight = contentHeight;
  //页面偏移
  var position = 0;
  //a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
  var imgWidth = 595.28 - (padding * 2);
  var imgHeight = 592.28 / contentWidth * contentHeight;

  var pageData = canvas.toDataURL('image/jpeg', 1.0);

  var pdf = new jsPDF('', 'pt', 'a4');

  //有两个高度需要区分，一个是html页面的实际高度，和生成pdf的页面高度(841.89)
  //当内容未超过pdf一页显示的范围，无需分页
  if (leftHeight < pageHeight) {
  pdf.addImage(pageData, 'JPEG', padding, 0, imgWidth, imgHeight );
  } else {
      while(leftHeight > 0) {
          pdf.addImage(pageData, 'JPEG', padding, position, imgWidth, imgHeight)
          leftHeight -= pageHeight;
          position -= 841.89;
          //避免添加空白页
          if(leftHeight > 0) {
            pdf.addPage();
          }
      }
  }

  await pdf.save(`${fileName}.pdf`, { returnPromise: true });
  return Promise.resolve(true)
}
```